<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationUsers extends Model
{
    use HasFactory;

    protected $table = 'application_users';

    protected $fillable = [
        'application_id',
        'user_id',
    ];
}
