<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{

    use SoftDeletes, HasFactory;

    protected $fillable = [
        'point_a',
        'point_b',
        'date',
        'status',
        'is_merged'
    ];

    public function applicationUsers(){
        return $this->hasMany(ApplicationUsers::class, 'application_id', 'id');
    }

}
