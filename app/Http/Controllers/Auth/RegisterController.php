<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends BaseController
{
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'city_id' => 'required|int',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        $user  = User::create($validatedData);
        $token = $user->createToken('authToken')->plainTextToken;

        return $this->successResponse('Successfully registered!', [
            'token' => $token,
        ]);
    }
}
