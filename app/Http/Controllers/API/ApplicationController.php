<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Models\Application;
use App\Services\ApplicationService;
use Illuminate\Http\Request;
use App\Enums\ApplicationStatusEnum;

class ApplicationController extends BaseController
{
    protected $applicationService;

    public function __construct(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
    }

    public function mergeApplications(Request $request)
    {
        $request->validate([
            'application_ids' => 'required|array',
        ]);

        try {
            $mergedApplication = $this->applicationService->mergeApplications($request->application_ids);
            return $this->successResponse('Заявки успешно объединены', $mergedApplication);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function createApplication(Request $request)
    {
        $this->validate($request, [
            'point_a' => 'required|string',
            'point_b' => 'required|string',
            'date' => 'required|date',
        ]);

        try {
            $this->applicationService->createApplication($request);

            return $this->successResponse('Заявка успешно отправлена', null, 201);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function getApplications()
    {
        return $this->successResponse('ok', Application::all(), 200);
    }

    public function rejectApplication($id)
    {
        $application         = Application::findOrFail($id);
        $application->status = ApplicationStatusEnum::REJECTED;
        $application->save();

        return $this->successResponse('Заявка успешно отклонена');
    }
}
