<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends BaseController
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cities',
        ]);

        $city = City::create([
            'name' => $request->input('name'),
        ]);

        return $this->successResponse('Город успешно создан', $city, 200);
    }

    public function get()
    {
        return $this->successResponse(City::all());
    }

}
