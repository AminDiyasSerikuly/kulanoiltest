<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends BaseController
{
    public function assignAdmin(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
        ]);

        $user           = User::find($request->user_id);
        $user->is_admin = true;
        $user->save();

        return $this->successResponse('Пользователь успешно назначен администратором');
    }
}
