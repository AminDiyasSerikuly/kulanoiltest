<?php

namespace App\Services;

use App\Models\Application;
use App\Models\ApplicationUsers;
use App\Enums\ApplicationStatusEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ApplicationService
{
    public function mergeApplications(array $application_ids)
    {
        try {
            return DB::transaction(function () use ($application_ids) {
                // Получение заявок с загрузкой связи applicationUsers
                $applications = Application::whereIn('id', $application_ids)->with('applicationUsers')->get();

                // Получение идентификаторов пользователей
                $applicationUsersIds = $applications->pluck('applicationUsers.*.user_id')->flatten();

                // Проверка на совпадение городов и дат
                $firstApplication = $applications->first();
                if (!$applications->every(function ($app) use ($firstApplication) {
                    return $app->point_a === $firstApplication->point_a &&
                        $app->point_b === $firstApplication->point_b &&
                        $app->date === $firstApplication->date;
                })) {
                    throw new \Exception('Невозможно объединить заявки. Пункты и дата не совпадают.');
                }

                // Создание объединенной заявки
                $mergedApplication = new Application([
                    'point_a' => $firstApplication->point_a,
                    'point_b' => $firstApplication->point_b,
                    'date' => $firstApplication->date,
                    'status' => ApplicationStatusEnum::IN_PROCCESS,
                    'is_merged' => true,
                ]);

                $mergedApplication->save();

                // Обновление application_id у связанных записей ApplicationUsers
                ApplicationUsers::whereIn('user_id', $applicationUsersIds)
                    ->whereIn('application_id', $application_ids)->update([
                        'application_id' => $mergedApplication->id,
                    ]);

                // Удаление объединяемых заявок
                Application::whereIn('id', $application_ids)->delete();
                return $mergedApplication;
            });
        } catch (\Exception $exception) {
            $error = $exception->getMessage();
            Log::channel('Api-Development')->info($error);

            throw new \Exception($error);
        }
    }

    public function createApplication(Request $request)
    {
        $user = Auth::user();
        try {
            return DB::transaction(function () use ($request, $user) {
                $application = Application::create(array_merge($request->all(),
                    ['status' => ApplicationStatusEnum::IN_PROCCESS]));

                ApplicationUsers::create([
                    'user_id' => $user->id,
                    'application_id' => $application->id,
                ]);
            });
        } catch (\Exception $exception) {
            $error = $exception->getMessage();
            Log::channel('Api-Development')->info($error);

            throw new \Exception($error);
        }
    }
}
