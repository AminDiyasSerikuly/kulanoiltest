<?php


namespace App\Enums;

enum ApplicationStatusEnum:string {
    case SUCCESS     = 'success';
    case IN_PROCCESS = 'in_process';
    case REJECTED    = 'rejected';
}
