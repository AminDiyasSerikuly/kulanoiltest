<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CreateApplicationTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateApplication()
    {
        // Создание пользователя
        $user = User::factory()->create();

        // Данные для создания заявки
        $requestData = [
            'point_a' => 'point a',
            'point_b' => 'point b',
            'date' => '2023-05-04',
        ];

        // Отправка POST-запроса для создания заявки
        $response = $this->actingAs($user)
            ->post('api/applications', $requestData);

        // Проверка кода статуса ответа
        $response->assertStatus(201);

        // Проверка наличия ключа "success" в JSON-ответе
        $response->assertJson(['success' => true]);

        // Проверка наличия созданной заявки в базе данных
        $this->assertDatabaseHas('applications', [
            'point_a' => 'point a',
            'point_b' => 'point b',
            'date' => '2023-05-04',
        ]);

        // Получение созданной заявки из базы данных
        $application = DB::table('applications')->where([
            'point_a' => 'point a',
            'point_b' => 'point b',
            'date' => '2023-05-04',
        ])->first();

        // Проверка наличия связи пользователя с заявкой в базе данных
        $this->assertDatabaseHas('application_users', [
            'user_id' => $user->id,
            'application_id' => $application->id
        ]);
    }

}
