<?php

namespace Tests\Unit;

use Database\Factories\UserAdminFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;

class AdminAssignmentTest extends TestCase
{
    use DatabaseTransactions;

    public function testAssignAdmin()
    {
        // Создаем тестового пользователя
        $user = User::factory()->create();
        $userAdmin = User::factory()->create();

        $userAdmin->is_admin = 1;
        $userAdmin->save();

        $this->actingAs($userAdmin);

        // Отправляем запрос на назначение пользователя администратором
        $response = $this->postJson('/api/admin/assign', ['user_id' => $user->id]);

        // Проверяем, что статус ответа равен 200 (успешный)
        $response->assertStatus(200);

        // Перезагружаем модель пользователя из базы данных
        $user->refresh();
        // Проверяем, что пользователь был назначен администратором
        $this->assertEquals(1, $user->is_admin);

    }

    public function testAssignAdminMissingUserId()
    {
        $userAdmin = User::factory()->create();

        $userAdmin->is_admin = 1;
        $userAdmin->save();

        $this->actingAs($userAdmin);
        // Отправляем запрос на назначение администратора без указания user_id
        $response = $this->postJson('/api/admin/assign');

        // Проверяем, что статус ответа равен 422 (неправильный запрос)
        $response->assertStatus(422);
    }

    public function testAssignAdminInvalidUserId()
    {

        $userAdmin = User::factory()->create();

        $userAdmin->is_admin = 1;
        $userAdmin->save();

        $this->actingAs($userAdmin);

        // Отправляем запрос на назначение администратора с несуществующим user_id
        $response = $this->postJson('/api/admin/assign', ['user_id' => 999]);

        // Проверяем, что статус ответа равен 422 (неправильный запрос)
        $response->assertStatus(422);
    }
}
