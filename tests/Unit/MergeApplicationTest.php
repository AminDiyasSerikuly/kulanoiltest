<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Application;
use App\Enums\ApplicationStatusEnum;

class MergeApplicationTest extends TestCase
{
    use DatabaseTransactions;

    public function testMergeApplications()
    {
        // Создание пользователя с правами администратора
        $userAdmin = User::factory()->create();

        // Установка пользователя как администратора
        $userAdmin->is_admin = 1;
        $userAdmin->save();

        // Аутентификация под администратором
        $this->actingAs($userAdmin);

        // Создание массива идентификаторов заявок
        $applicationIds = [];

        // Создание двух заявок
        $applications = Application::factory()->count(2)->create();

        foreach ($applications as $application) {
            $applicationIds[] = $application->id;
        }

        // Выполнение операции объединения заявок
        $response = $this->actingAs($userAdmin)->post('api/applications/merge', [
            'application_ids' => $applicationIds,
        ]);

        // Проверка кода статуса ответа
        $response->assertStatus(200);

        // Проверка, что объединенная заявка была создана
        $this->assertDatabaseHas('applications', [
            'point_a' => $applications->first()->point_a,
            'point_b' => $applications->first()->point_b,
            'date' => $applications->first()->date,
            'status' => ApplicationStatusEnum::IN_PROCCESS,
            'is_merged' => true,
        ]);

        // Проверка, что исходные заявки отсутствуют в базе данных
        foreach ($applicationIds as $id) {
            $this->assertDatabaseMissing('applications', [
                'id' => $id,
                'deleted_at' => null
            ]);
        }
    }

}
