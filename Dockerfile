FROM php:8.2.4-fpm-alpine3.17

# Установка зависимостей
RUN apk --no-cache add pcre-dev ${PHPIZE_DEPS} \
      && apk add --update libzip-dev curl-dev \
      && apk add --no-cache mysql-client\
      && docker-php-ext-install pdo_mysql\
      && pecl install redis \
      && docker-php-ext-enable redis \
      && apk del pcre-dev ${PHPIZE_DEPS} \
      && rm -rf /tmp/pear \
      && apk add bash && apk add nano && apk add htop && apk add curl


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Создание директории для приложения
WORKDIR /application


# Открытие портов
EXPOSE 80

# Set timezone
ENV TZ=Asia/Almaty
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN printf "[PHP]\ndate.timezone = $TZ\n" > /usr/local/etc/php/conf.d/tzone.ini
