<?php

use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\ApplicationController;
use App\Http\Controllers\API\CityController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@register');
Route::post('/login', 'App\Http\Controllers\Auth\LoginController@login');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('applications', [ApplicationController::class, 'createApplication']);

    Route::group(['middleware' => 'admin'], function () {
        Route::post('admin/assign', [AdminController::class, 'assignAdmin']);

        Route::post('applications/{id}/reject', [ApplicationController::class, 'rejectApplication']);
        Route::post('applications/merge', [ApplicationController::class, 'mergeApplications']);
        Route::get('applications', [ApplicationController::class, 'getApplications']);

        Route::post('cities', [CityController::class, 'store']);
        Route::get('cities', [CityController::class, 'get']);
    });
});



