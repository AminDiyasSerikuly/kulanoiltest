<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'point_a' => 'Astana, Esil',
            'point_b' => 'Almaty, Zhandosova 35',
            'date' => '2023-05-01',
            'status' => $this->faker->randomElement(['in_process']),
            'is_merged' => false,
        ];
    }
}
